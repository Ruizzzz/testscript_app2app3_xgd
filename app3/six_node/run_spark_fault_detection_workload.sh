#!/usr/bin/env bash
kafkaAddress=172.19.0.200:9888,172.19.0.201:9888,172.19.0.203:9888,172.19.0.205:9888,172.19.0.206:9888

#kafkaAddress=10.11.6.126:9092
# 输入topic
inputTopic=app38p30k
# 总核数=executor数量
repartition=6
# detection的模式
appNum=3
# shuffle算子的分区数量
shuffleNum=1
# Executor数量
# 执行模式 CPU GPU or TF
useServer=cpu
# 执行器数量，用于微调性能，若想使用两个GPU至少为两个
ExecutorNum=6
#app1,app2的detection的类型
detectionType=1
#图片的数量
pictureNum=30000
outputTopic=app3sixnodewrite
# 最少核数，这里无实际意义，兼容下面代码
baselineCore=${shuffleNum}
## 输入速率
##fps=$6
# 每个Executor核数
perCore=1
# 机器可提供内存量
singleMem=160
# 每个Executor的内存
#perMem=`expr $singleMem / $ExecutorNum`G
# 与 执行器数量对比
# 计算每一个执行器内的CPU核数
if [ $repartition -lt $baselineCore ]
then
perCore=`expr $baselineCore / $ExecutorNum `
echo task = `expr $baselineCore / $ExecutorNum `
elif [ $repartition -lt $ExecutorNum ]
then
  ExecutorNum=$repartition
else
# 确定每一个执行器进程的核数
for a in $(seq 18)
do
   if [[ `expr $a \* $ExecutorNum ` -ge $repartition ]]
    then
       perCore=$a
       break
fi
done
fi
#for a in $(seq 10)
#do
#   if [[ `expr $a \* $ExecutorNum ` -ge $repartition ]]
#   then
#       perCore=$a
#       break
#   fi
#done
totalCore=`expr $perCore \* $ExecutorNum`
#perMem=`expr $singleMem / $ExecutorNum`G
perMem=45G
# 每个Executor内存
echo inputTopic = $inputTopic
echo totalCore = $totalCore
echo perCore = $perCore
echo repartition = $repartition
echo perMem = $perMem
#exit
# master地址，HRM要以mesos开头
#spark 提交
#master=spark://ds126:7977
#master=spark://master00:7077
master=mesos://172.19.0.201:5550
#master=spark://master00:7077
#hrm提交
#master=mesos://10.11.6.126:5550
# driver内存
driverMem=20G
# 执行模式 client or cluster
deployMod=client
# 入口类
mainClass=app3_dag
# gpuindex (已废弃)
GPUIndex=0
# 分区数量 (已废弃)
# jar包URL
jarURl=/root/project2-irdag-private/app1-dag/target/app1-dag-1.0.jar

#hdfsIp=hdfs://ds126:9105
# 删除原有检查点URL (流计算)
#/root/map123/hadoop-2.7.7/bin/hadoop dfs -rmr $hdfsIp$ckDir


# /root/map/hadoop-2.7.7/bin/hadoop dfs -mkdir hdfs://10.11.1.208:9005/$ckDir
#rm /root/map123/spark-2.4.3-bin-hadoop2.7/app1-dag-1.0.jar
#SPARK_HOME=/root/spark-2.4.3-bin-hadoop2.7
SPARK_HOME=/root/map123/spark-2.4.3-bin-hadoop2.7/

$SPARK_HOME/bin/spark-submit \
        --deploy-mode ${deployMod} \
        --driver-memory ${driverMem} \
        --total-executor-cores ${totalCore} \
        --executor-cores ${perCore} \
        --executor-memory ${perMem} \
        --conf spark.driver.maxResultSize=20g \
        --master ${master} \
        --class ${mainClass} ${jarURl} ${kafkaAddress} ${inputTopic} ${repartition} ${appNum} ${shuffleNum} ${useServer} ${detectionType} ${pictureNum} ${outputTopic}
ssh -t root@slave01 rm /root/tools/app123-docker/map123/spark-2.4.3-bin-hadoop2.7/app1-dag-1.0.jar
ssh -t root@slave02 rm /root/tools/app123-docker/map123/spark-2.4.3-bin-hadoop2.7/app1-dag-1.0.jar
ssh -t root@slave03 rm /root/tools/app123-docker/map123/spark-2.4.3-bin-hadoop2.7/app1-dag-1.0.jar
ssh -t root@slave04 rm /root/tools/app123-docker/map123/spark-2.4.3-bin-hadoop2.7/app1-dag-1.0.jar
ssh -t root@slave05 rm /root/tools/app123-docker/map123/spark-2.4.3-bin-hadoop2.7/app1-dag-1.0.jar
ssh -t root@slave06 rm /root/tools/app123-docker/map123/spark-2.4.3-bin-hadoop2.7/app1-dag-1.0.jar